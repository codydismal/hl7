import Vue from 'vue'
import Router from 'vue-router'


const routerOptions = [
  {path: '/message-types', component: 'MessageTypeTable/MessageTypeTable'},
  {path: '/events', component: 'Events/EventsTable'},
  {path: '/segment', component: 'Table'},
  {path: '/', component: 'MainView'},
  {path: '/segment/fields', component: 'FieldTable'},
  {path: '*', component: 'NotFound'}
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`@/components/main/${route.component}.vue`),
  }
})

Vue.use(Router)

export default new Router({
  routes,
  mode: 'history'
})
