// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

import Footer from './components/Footer'
import Toolbar from './components/ToolBar'
import VersionSelector from './components/sidebar/VersionSelecter'
import NewDataSetModal from './components/sidebar/NewDataSetModal'
import Table from './components/main/Table'
import EventEditor from './components/main/EventEditor/EventEditor'

import './main.css'

Vue.use(Vuetify)

Vue.component('my-footer', Footer)
Vue.component('toolbar', Toolbar)
Vue.component('version-selecter', VersionSelector)
Vue.component('new-dataset-modal', NewDataSetModal)
Vue.component('segment-table', Table)
Vue.component('event-editor', EventEditor)

Vue.config.productionTip = false

import axios from 'axios'

Vue.prototype.$http = axios
Vue.prototype.$baseAddr = 'http://localhost:5000'
Vue.prototype.$dataset = {};

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})


