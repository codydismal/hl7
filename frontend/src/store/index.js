import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    dataset: "",
    segmentName: "",
    segmentId: -1,

    messageTypeList: [],
    segmentList: [],

    fieldList: [],
    eventList: [],


    messageTypeName: '',
    messageTypeId: -1,

    actualViewMessage: 'Strona główna',

    leftPanelVisible: true,

    eventEditorVisible: false,
    messageEditorVisible: false,
  },
  mutations: {
    setDatasetName(state, name) {
      state.dataset = name;
    },
    setSegmentNameAndId(state, segmentNameAndId) {
      state.segmentName = segmentNameAndId[0];
      state.segmentId = segmentNameAndId[1];
    },
    setMessageTypeAndId(state, setMessageTypeAndId) {
      state.messageTypeName = setMessageTypeAndId[0];
      state.messageTypeId = setMessageTypeAndId[1];
    }
  },
  getters: {
    isDatasetSelected: state => {
      return state.dataset.length > 0
    },
    isSegmentSelected: state => {
      return state.segmentName && state.dataset
    }
  }
});

