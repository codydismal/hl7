from backend.logic import fields
from backend.logic.database import Connection


def create(data):
    conn = Connection(data.get('dataset'))
    order = -1
    conn.non_select('DELETE FROM message_structure WHERE str_evt_id={}'.format(data.get('evt_id')))
    for seg in data['segments']:
        order += 1
        conn.non_select("INSERT INTO message_structure (`str_order`, `str_evt_id`, `str_seg_id`) \
VALUES ({0}, {1}, {2})".format(order, data.get('evt_id'), seg.get('seg_id')))


def get(data):
    conn = Connection(data.get('dataset'))
    structs = conn.select('SELECT * FROM message_structure WHERE str_evt_id={}'.format(data.get('eventId')))
    res = []
    for seg in structs:
        res.append(
            conn.select('SELECT * FROM segment WHERE seg_id={}'.format(seg.get('str_seg_id')))[0]
        )
    return res
