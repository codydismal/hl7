from backend.logic.database import Connection


def get_events(data):
    conn = Connection(data.get('dataset'))
    return conn.select('SELECT * FROM event WHERE evt_mst_id={}'.format(data.get('messageTypeId')))


def create_event(data):
    conn = Connection(data.get('dataset'))
    conn.non_select("INSERT INTO event (`evt_name`, `evt_description`, `evt_mst_id`) VALUES \
    ('{0}', '{1}', {2})".format(data.get('eventName'), data.get('eventDescription'), data.get('messageTypeId')))
    return get_events(data)


def delete_event(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('DELETE FROM event WHERE evt_id={}'.format(data.get('eventId')))
    return get_events(data)


def update_event(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('UPDATE event SET evt_name="{0}", evt_description="{1}" WHERE evt_id={2}'.format(
        data.get('eventName'), data.get('eventDescription'), data.get('eventId')))
    return get_events(data)
