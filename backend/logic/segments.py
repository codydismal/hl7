from backend.logic.database import Connection


def create_segment(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('INSERT INTO segment (`seg_name`, `seg_description`) VALUES ("{0}", "{1}")'.format(
        data.get('segmentName'), data.get('segmentDescription')))


def update_segment(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('UPDATE segment SET seg_name="{0}", seg_description="{1}" WHERE seg_id={2}'.format(
        data.get('segmentName'), data.get('segmentDescription'), data.get('segmentId')))


def delete_segment(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('DELETE FROM segment WHERE seg_id={}'.format(data.get('segmentId')))
