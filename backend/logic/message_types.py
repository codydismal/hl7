from backend.logic.database import Connection


def get_types_by_dataset(data):
    conn = Connection(data.get('dataset'))
    return conn.select('SELECT * FROM message_type'.format(data.get('segmentId')))


def create_message_type(data):
    conn = Connection(data.get('dataset'))
    conn.non_select("INSERT INTO message_type (`mst_name`, `mst_description`) VALUES \
('{0}', '{1}')".format(data.get('messageTypeName'), data.get('messageTypeDescription')))
    return get_types_by_dataset(data)


def delete_message_type(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('DELETE FROM message_type WHERE mst_id={}'.format(data.get('messageTypeId')))


def update_message_type(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('UPDATE message_type SET mst_name="{0}", mst_description="{1}" WHERE mst_id={2}'.format(
        data.get('messageTypeName'), data.get('messageTypeDescription'), data.get('messageTypeId')))

