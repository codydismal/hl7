import sqlite3
import os
from backend.manage import ROOT_DIR


class Connection:
    c = None
    conn = None
    db_name = None

    @staticmethod
    def _dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def __init__(self, db_name):
        if not db_name.endswith('.hl7'):
            db_name += '.hl7'

        self.conn = sqlite3.connect(os.path.join(ROOT_DIR, 'databases', db_name))
        self.conn.row_factory = self._dict_factory
        self.c = self.conn.cursor()

    def select(self, query):
        result = [row for row in self.c.execute(query)]
        self.conn.commit()
        return result

    def non_select(self, query):
        self.c.execute(query)
        self.conn.commit()
