import os

from backend import manage
from backend.logic.database import Connection
from os import listdir
import sqlite3

db_list_path = os.path.join(manage.ROOT_DIR, 'databases')


def load_all_hl7_datasets():
    files = [{'title': f, 'onClick': 'openDataSet("{}")'.format(f)}
             for f in listdir(db_list_path)
             if f.endswith('.hl7')]
    return files


def create_hl7_dataset(name):
    conn = sqlite3.connect(os.path.join(db_list_path, '{}.hl7'.format(name)))
    c = conn.cursor()
    try:
        c.execute('''CREATE TABLE hl7
                     (hl7_version TEXT, 
                     hl7_release_date DATE)''')

        c.execute('''CREATE TABLE message_type
                                 (mst_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                 mst_name TEXT, 
                                 mst_description TEXT)''')

        c.execute('''CREATE TABLE event
                                         (evt_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                         evt_name TEXT, 
                                         evt_description TEXT,
                                         evt_mst_id INT, 
                                         FOREIGN KEY(evt_mst_id) REFERENCES message_type(mst_id))''')

        c.execute('''CREATE TABLE message_structure
                                                 (str_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                                                 str_order INT NOT NULL,
                                                 str_evt_id INT, 
                                                 str_seg_id INT, 
                                                 FOREIGN KEY(str_evt_id) REFERENCES event(evt_id),
                                                 FOREIGN KEY(str_seg_id) REFERENCES segment(seg_id))''')

        c.execute('''CREATE TABLE segment
                         (seg_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                         seg_number INTEGER, 
                         seg_name TEXT, 
                         seg_description TEXT)''')

        c.execute('''CREATE TABLE field
                         (fld_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                         fld_order INTEGER NOT NULL, 
                         fld_name TEXT UNIQUE, 
                         fld_description TEXT, 
                         fld_seg_id INT, 
                         FOREIGN KEY(fld_seg_id) REFERENCES segment(seg_id))''')

        conn.commit()
    except sqlite3.OperationalError:
        return {'status': "Error", 'message': "This dataset is already created."}
    conn.close()
    return {'status': "Success", 'message': "Dataset created."}


def load_hl7_dataset(name):
    conn = Connection(name)
    segment_headers = conn.select('SELECT * FROM segment')

    return {
        'headers': segment_headers
    }
