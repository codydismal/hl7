from backend.logic.database import Connection


def _fix_orders(data):
    conn = Connection(data.get('dataset'))
    segments = get_fields_by_segment(data)
    i = 0
    for el in segments:
        conn.non_select('UPDATE field SET fld_order={0} WHERE fld_id={1}'.format(i, el.get('fld_id')))
        i += 1


def get_fields_by_segment(data):
    conn = Connection(data.get('dataset'))
    return conn.select('SELECT * FROM field WHERE fld_seg_id="{}" ORDER BY fld_order'.format(data.get('segmentId')))


def create_field(data):
    conn = Connection(data.get('dataset'))
    last_segment = conn.select(
        'SELECT max(fld_order) FROM field WHERE fld_seg_id="{}"'.format(data.get('segmentId')))[0]['max(fld_order)']
    if last_segment is None:
        last_segment_order = 0
    else:
        last_segment_order = last_segment + 1
    conn = Connection(data.get('dataset'))
    conn.non_select(
        'INSERT INTO field (`fld_name`, `fld_description`,\
 `fld_order`, `fld_seg_id`) VALUES ("{0}", "{1}", {2}, {3})'.format(
            data.get('fieldName'), data.get('fieldDescription'), last_segment_order, data.get('segmentId')))
    return get_fields_by_segment(data)


def delete_field(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('DELETE FROM field WHERE fld_id={}'.format(data.get('fieldId')))
    _fix_orders(data)


def update_field(data):
    conn = Connection(data.get('dataset'))
    conn.non_select('UPDATE field SET fld_name="{0}", fld_description="{1}" WHERE fld_id={2}'.format(
        data.get('fieldName'), data.get('fieldDescription'), data.get('fieldId')))
