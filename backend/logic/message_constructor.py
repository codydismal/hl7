from backend.logic.database import Connection


def load(data):
    conn = Connection(data.get('dataset'))
    result = []

    segments = conn.select("SELECT * FROM message_structure WHERE str_evt_id={}".format(data.get('eventId')))
    for seg in segments:
        fields = conn.select("SELECT * FROM field WHERE fld_seg_id={} ORDER BY fld_order".format(seg['str_seg_id']))
        segment_info = conn.select("SELECT * FROM segment WHERE seg_id={}".format(seg['str_seg_id']))[0]
        fields = [{**field, 'userValue': ''} for field in fields]
        result.append(
            {
                'segmentName': segment_info['seg_name'],
                'segmentDescription': segment_info['seg_description'],
                'fields': fields
            }
        )
    return result
