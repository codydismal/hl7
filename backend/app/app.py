from flask import Flask, render_template, jsonify, request, session
from flask_cors import CORS
from random import *

from flask_session import Session

from backend.logic import hl7_version, segments, fields, message_types, events, message_structure, message_constructor

app = Flask(__name__, static_folder="../../dist/static", template_folder="../../dist")

app.config['SECRET_KEY'] = "SUPER_RANDOM"
app.config.from_object(__name__)

cors = CORS(app, resources={r"*": {"origins": "*"}})


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return render_template("index.html")


@app.route('/hl7files', methods=['GET'])
def list_all_hl7_files():
    response = hl7_version.load_all_hl7_datasets()
    return jsonify(response)


@app.route('/hl7file', methods=['POST'])
def create_new_hl7_file():
    data = request.get_json()
    response = hl7_version.create_hl7_dataset(data.get('name'))
    return jsonify(response)


@app.route('/hl7file', methods=['GET'])
def get_hl7_file():
    data = request.args
    response = hl7_version.load_hl7_dataset(data.get('name'))
    return jsonify(response)


@app.route('/message-segment', methods=['POST'])
def create_new_hl7_segment():
    data = request.get_json()
    response = segments.create_segment(data)
    return jsonify(response)


@app.route('/message-segment', methods=['PUT'])
def update_new_hl7_segment():
    data = request.get_json()
    response = segments.update_segment(data)
    return jsonify(response)


@app.route('/message-segment', methods=['DELETE'])
def delete_new_hl7_segment():
    data = request.args
    response = segments.delete_segment(data)
    return jsonify(response)


@app.route('/message-segment', methods=['GET'])
def get_new_hl7_segment():
    data = request.args
    response = fields.get_fields_by_segment(data)
    return jsonify(response)


@app.route('/field', methods=['POST'])
def create_new_hl7_field():
    data = request.get_json()
    response = fields.create_field(data)
    return jsonify(response)


@app.route('/field', methods=['DELETE'])
def delete_hl7_field():
    data = request.args
    response = fields.delete_field(data)
    return jsonify(response)


@app.route('/field', methods=['PUT'])
def update_hl7_field():
    data = request.get_json()
    response = fields.update_field(data)
    return jsonify(response)


# Endpoints for message types
@app.route('/message-type', methods=['GET'])
def get_message_types():
    data = request.args
    response = message_types.get_types_by_dataset(data)
    return jsonify(response)


@app.route('/message-type', methods=['POST'])
def create_new_message_type():
    data = request.get_json()
    response = message_types.create_message_type(data)
    return jsonify(response)


@app.route('/message-type', methods=['DELETE'])
def delete_message_type():
    data = request.args
    response = message_types.delete_message_type(data)
    return jsonify(response)


@app.route('/message-type', methods=['PUT'])
def update_message_type():
    data = request.get_json()
    response = message_types.update_message_type(data)
    return jsonify(response)


# Endpoints for events
@app.route('/event', methods=['GET'])
def get_events():
    data = request.args
    response = events.get_events(data)
    return jsonify(response)


@app.route('/event', methods=['POST'])
def create_new_event():
    data = request.get_json()
    response = events.create_event(data)
    return jsonify(response)


@app.route('/event', methods=['DELETE'])
def delete_event():
    data = request.args
    response = events.delete_event(data)
    return jsonify(response)


@app.route('/event', methods=['PUT'])
def update_event():
    data = request.get_json()
    response = events.update_event(data)
    return jsonify(response)


# Endpoints for message structure
@app.route('/message-structure', methods=['GET'])
def get_message_structure():
    data = request.args
    response = message_structure.get(data)
    return jsonify(response)


@app.route('/message-structure', methods=['POST'])
def create_new_message_structure():
    data = request.get_json()
    response = message_structure.create(data)
    return jsonify(response)


@app.route('/message-construction', methods=['GET'])
def load_message_construction():
    data = request.args
    response = message_constructor.load(data)
    return jsonify(response)

@app.route('/api/random')
def random_number():
    response = {
        'randomNumber': randint(1, 100)
    }
    return jsonify(response)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response
